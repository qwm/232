package tasks.model;

import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

import java.util.*;

public class TasksOperations {

    private final List<Task> tasks;
    private static final Logger log = Logger.getLogger(TasksOperations.class.getName());

    public TasksOperations(ObservableList<Task> tasksList){
        tasks=new ArrayList<>();
        tasks.addAll(tasksList);
    }

    public Iterable<Task> incoming(Date start, Date end){
        log.info(start);
        log.info(end);
        ArrayList<Task> incomingTasks = new ArrayList<>();
        for (Task t : tasks) {
            Date nextTime = t.nextTimeAfter(start);
            log.info(nextTime);
            if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                incomingTasks.add(t);
                log.info(t.getTitle());
            }
        }
        return incomingTasks;
    }
}
