package tasks.model;

import org.apache.log4j.Logger;
import tasks.services.TaskIO;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Task implements Serializable, Cloneable {
    private String title;
    private String descriere;
    private Date start;
    private Date end;
    private int interval;
    private boolean active;
    private boolean repetitive;

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public boolean isRepetitive() {
        return repetitive;
    }

    public void setRepetitive(boolean repetitive) {
        this.repetitive = repetitive;
    }

    private static final Logger log = Logger.getLogger(Task.class.getName());
    private  SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public  SimpleDateFormat getDateFormat(){
        return sdf;
    }


    public Task(String title, String descriere, Date start, Date end,boolean repetitive, int interval){

        if (start.getTime() < 0 || end.getTime() < 0) {
            log.error("time below bound");
            throw new IllegalArgumentException("Data invalida");
        }
        if (interval < 0){
            log.error("Interval invalid");
            throw new IllegalArgumentException("Interval invalid");
        }
        else if (interval == 0 && repetitive) {
            log.error("Interval invalid");
            throw new IllegalArgumentException("Interval de repetitie cu valoare 0 la un task repetitiv");
        }
        else if (interval > 0 && !repetitive) {
            log.error("Interval invalid");
            throw new IllegalArgumentException("Interval de repetitie cu valoare diferita de 0 la un task nerepetitiv");
        }

        this.title = title;
        this.start = start;
        this.end = end;
        this.interval = interval;
        this.active = true;
        this.repetitive = repetitive;
        this.descriere = descriere;
    }

    public String getTitle() {
        return title;
    }

    public boolean isActive(){
        return this.active;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public Date getStartTime() {
        return start;
    }

    public Date getEndTime() { return end; }

    public int getRepeatInterval(){ return  interval; }

    public void update(String title, String descriere, Date start, Date end,boolean repetitive, int interval){
        String err = "";
        if (start.getTime() < 0 || end.getTime() < 0) {
            log.error("time below bound");
            err += "Data invalida \n";
        }
        if (title.isEmpty()){
            log.error("Titlu invalid");
            err += "Titlul nu poate fi gol \n";
        }
        if (interval < 0){
            log.error("Interval invalid");
            err += "Interval invalid \n";
        }
        else if (interval == 0 && repetitive) {
            log.error("Interval invalid");
            err += "Interval de repetitie cu valoare 0 la un task repetitiv \n";
        }
        else if (interval > 0 && !repetitive) {
            log.error("Interval invalid");
            err += "Interval de repetitie cu valoare diferita de 0 la un task nerepetitiv \n";
        }

        if(!err.isEmpty()){
            throw new IllegalArgumentException(err);
        }

        this.title = title;
        this.start = start;
        this.end = end;
        this.interval = interval;
        this.active = true;
        this.repetitive = repetitive;
        this.descriere = descriere;
    }






    public Date nextTimeAfter(Date current){
        Date next = null;
        // end of task
        if (current.after(end) || current.equals(end)) return null;

        // first time of the task
        if (current.before(start) || current.equals(start)) return start;

        // next time for repetitive tasks
        for (long i = start.getTime() + interval; repetitive && i <= end.getTime(); i += interval) {
            if (i >= current.getTime()) {
                next = new Date(i);
                break;
            }
            else if (current.after(new Date(i))) {
                break;
            }
        }
        return next;
    }







    //duplicate methods for TableView which sets column
    // value by single method and doesn't allow passing parameters
    public String getFormattedDateStart(){
        return sdf.format(start);
    }
    public String getFormattedDateEnd(){
        return sdf.format(end);
    }
    public String getFormattedRepeated(){
        if (isRepetitive()){
            String formattedInterval = TaskIO.getFormattedInterval(interval);
            return "Every " + formattedInterval;
        }
        else {
            return "No";
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (!start.equals(task.start)) return false;
        if (!end.equals(task.end)) return false;
        if (interval != task.interval) return false;
        if (active != task.active) return false;
        return title.equals(task.title);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + interval;
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", interval=" + interval +
                ", active=" + active +
                '}';
    }

    @Override
    protected Task clone() throws CloneNotSupportedException {
        Task task  = (Task)super.clone();
        task.start = (Date)this.start.clone();
        task.end = (Date)this.end.clone();
        return task;
    }
}


