package tasks;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TaskValidatorTest {

    private static final org.apache.log4j.Logger log = Logger.getLogger(TaskValidatorTest.class.getName());
    public static SimpleDateFormat sdf;
    public static Calendar calendar;
    public static Date start, end, time;
    public static Task task;

    @BeforeAll
    static void initAll(){
        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        calendar = Calendar.getInstance(); // calendar.set(y,m,d,h,m,s);  first month = 0
        start = new Date();
        end = new Date();
        time = new Date();
    }

    @Test
    @DisplayName("ECP interval valid")
    @Tag("valid")
     void test_ECP_valid1(){
        calendar.set(2021, Calendar.MARCH, 20, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try{
            task = new Task("Task1", "descriere task valid", start, end,true, 10);
            assert true;
        } catch (Exception e){
            assert false;
        }
    }

    @Test
    @DisplayName("ECP interval invalid")
    @Tag("invalid")
     void test_ECP_invalid1(){
        calendar.set(2021, Calendar.MARCH, 20, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();

        try {
            task = new Task("Task2", "descriere task invalid", start, end,true, -100);
            assert false;
        }catch(Exception e){
            assert (e.getMessage().equals("Interval invalid"));
        }
    }

    @Test
    @DisplayName("ECP data de inceput valida")
    @Tag("valid")
     void test_ECP_valid2(){
        calendar.set(2021, Calendar.APRIL, 21, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.OCTOBER, 20, 12, 30, 0);
        end = calendar.getTime();
        try{
            task = new Task("Task3", "descriere task valid", start, end,true,  10);
            assert true;
        } catch (Exception e){
            assert false;
        }
    }

    @Test
    @DisplayName("ECP data de inceput invalida")
    @Tag("invalid")
     void test_ECP_invalid2(){
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try {
            task = new Task("Task2", "descriere task invalid", new Date(-1), end,true,10);
            assert false;
        }catch(Exception e){
            assert (e.getMessage().equals("Data invalida"));
        }
    }



    @DisplayName("BVA interval de repetitie valid")
    @Tag("valid")
    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void test_BVA12(String repetitive) {
        calendar.set(2021, Calendar.APRIL, 21, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try {
            task = new Task("Task2", "descriere task invalid", start, end, Boolean.parseBoolean(repetitive), 1);
            assert Boolean.parseBoolean(repetitive);
        } catch (Exception e) {
            assert e.getMessage().equals("Interval de repetitie cu valoare diferita de 0 la un task nerepetitiv");
        }
    }


    @DisplayName("BVA interval de repetitie valid")
    @Tag("valid")
    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void test_BVA34(String repetitive) {
        calendar.set(2021, Calendar.APRIL, 21, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try {
            task = new Task("Task2", "descriere task invalid", start, end, Boolean.parseBoolean(repetitive), 0);
            assert !Boolean.parseBoolean(repetitive);
        } catch (Exception e) {
            assert e.getMessage().equals("Interval de repetitie cu valoare 0 la un task repetitiv");
        }
    }

    @Test
    @DisplayName("BVA interval de repetitie invalid")
    @Tag("invalid")
    void test_BVA_invalid3() {
        calendar.set(2021, Calendar.APRIL, 21, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2021, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try {
            task = new Task("Task2", "descriere task invalid", start, end, false, -10);
            assert false;
        } catch (Exception e) {
            assert (e.getMessage().equals("Interval invalid"));
        }
    }

    @Disabled ("Caz de testare acoperit de test_ECP_valid1")
    @Test
    void testInutil(){
        calendar.set(2021, Calendar.MARCH, 20, 10, 30, 0);
        start = calendar.getTime();
        calendar.set(2022, Calendar.MARCH, 20, 12, 30, 0);
        end = calendar.getTime();
        try{
            task = new Task("Task1", "descriere task valid", start, end,true, 50);
            assert true;
        } catch (Exception e){
            assert false;
        }
    }

}
