package tasks;

import org.junit.jupiter.api.*;
import tasks.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


 class TaskNextTimeAfterDateTest {
    public static Task general_task, repetitive_task;

    @BeforeAll
    static void initAll(){
        general_task = new Task("Task1", "descriere task valid", new Date(100), new Date(1000), false, 0);
        repetitive_task = new Task("Task2", "descriere task valid", new Date(50), new Date(100), true, 30);
    }

    @Test
    @DisplayName("WBT interval valid")
    @Tag("valid")
     void test_WB_valid(){
        assert general_task.nextTimeAfter(new Date(90)).equals(new Date(100));
        assert repetitive_task.nextTimeAfter(new Date(80)).equals(new Date(80));
    }

    @Test
    @DisplayName("WBT interval invalid")
    @Tag("invalid")
     void test_WB_invalid(){
        assert general_task.nextTimeAfter(new Date(110)) == null;
        assert general_task.nextTimeAfter(new Date(1000)) == null;
        assert repetitive_task.nextTimeAfter(new Date(90)) == null;

    }

}
